import React from 'react';
import Geolocation from '@react-native-community/geolocation';

import {StyleSheet, Text, View} from 'react-native';
import Weather from './src/Weather';

class App extends React.Component {
  state = {
    isLoading: true,
    location: null,
    temperature: 0,
    weatherCondition: null,
    error: null,
    API_KEY: '1cc483397a1e0db0232d03f32c3b983b',
  };

  componentDidMount() {
    Geolocation.getCurrentPosition(position => {
      const location = JSON.stringify(position.coords);
      this.setState({location});

      this.fetchWeather(position.coords.latitude, position.coords.longitude);
    });
  }

  fetchWeather(lat = 25, lon = 25) {
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${this.state.API_KEY}&units=metric`,
    )
      .then(res => res.json())
      .then(json => {
        this.setState({
          temperature: json.main,
          weatherCondition: json.weather[0].main,
          isLoading: false,
        });
      });
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.isLoading ? (
          <Text>Fetching The Weather</Text>
        ) : (
          <Weather
            weather={this.state.weatherCondition}
            temperature={this.state.temperature}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
