import React from 'react';

import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {weatherConditions, calcTempCelsius, calcTempFahrenheit} from './util';

const Weather = ({weather, temperature}) => {
  const tempCurrent = temperature.temp;

  const tmpC = calcTempCelsius(tempCurrent);
  const tempMinC = calcTempCelsius(temperature.temp_min);
  const tempMaxC = calcTempCelsius(temperature.temp_max);

  const tmpF = calcTempFahrenheit(tempCurrent);
  const tempMinF = calcTempFahrenheit(temperature.temp_min);
  const tempMaxF = calcTempFahrenheit(temperature.temp_max);

  return (
    <View
      style={[
        styles.weatherContainer,
        {backgroundColor: weatherConditions[weather].color},
      ]}>
      <View style={styles.headerContainer}>
        <Text style={styles.tempTextHeader}>{tmpC}˚C</Text>
      </View>

      <View style={styles.middleContainer}>
        <Text style={styles.tempText}>-{tempMinC}˚C</Text>
        <Text style={styles.tempText}>+{tempMaxC}˚C</Text>
      </View>

      <View style={styles.headerContainer}>
        <Text style={styles.tempTextHeader}>{tmpF}˚F</Text>
      </View>

      <View style={styles.middleContainer}>
        <Text style={styles.tempText}>-{tempMinF}˚F</Text>
        <Text style={styles.tempText}>+{tempMaxF}˚F</Text>
      </View>

      <View style={styles.bodyContainer}>
        <Icon size={72} name={weatherConditions[weather].icon} color={'#fff'} />
        <Text style={styles.title}>{weatherConditions[weather].title}</Text>
        <Text style={styles.subtitle}>
          {weatherConditions[weather].subtitle}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  weatherContainer: {
    flex: 1,
  },
  headerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  middleContainer: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  tempTextHeader: {
    fontSize: 48,
    color: '#fff',
  },
  tempText: {
    fontSize: 24,
    color: '#fff',
  },
  bodyContainer: {
    flex: 2,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    paddingLeft: 25,
    marginBottom: 40,
  },
  title: {
    fontSize: 48,
    color: '#fff',
  },
  subtitle: {
    fontSize: 24,
    color: '#fff',
  },
});

export default Weather;
